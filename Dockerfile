# Build stage
FROM golang:alpine AS build-env
WORKDIR $GOPATH/src/budget-tracker
COPY . .
RUN apk add --no-cache git ca-certificates &&\
    go get -d -v
RUN GOOS=linux CGO_ENABLED=0 go build -a -ldflags '-w -s' -o /go/bin/budget-tracker .

# Run stage
FROM scratch
COPY --from=build-env /go/bin/budget-tracker /go/bin/budget-tracker
COPY --from=build-env /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/ca-certificates.crt
COPY credentials.json config.toml ./
ENTRYPOINT [ "/go/bin/budget-tracker" ]