IMAGE ?= budget-tracker
TAG ?= latest

all: build run

build:
	@docker build -t ${IMAGE}:${TAG} .

run:
	@docker run --rm -it ${IMAGE}:${TAG}

clean:
	@docker rmi ${IMAGE}:${TAG}

.PHONY: all build run
