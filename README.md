# budget-tracker

This repository holds a Go application that interfaces with Google Sheets using [spreadsheet](https://github.com/Iwark/spreadsheet). It is used to input expenses that will be tracked and graphed in Google Sheets.

## User guide

### Prerequisites

The optional tools below can be installed to make it easier to use this product:

  - Make (for running `make` commands from the included **Makefile**)
  - Docker (included **Dockerfile** creates the Go environment for you, but you could run Go directly on your system if you prefer)

### Preparation

  1. `git clone https://gitlab.com/mitchenielsen/budget-tracker.git`
  2. Download your credentials from Google using the [guide](http://gspread.readthedocs.org/en/latest/oauth2.html) and store the JSON file in the root of this repo.
  3. Tweak the values in **config.toml** as needed.

### Usage

Run `make`, which will ensure the Docker image is up to date and prompt you on the command line for the data to append to the spreadsheet. Run that any time you want to add new rows. That's it!