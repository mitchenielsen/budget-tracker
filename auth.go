package main

import (
	"io/ioutil"

	"golang.org/x/net/context"
	"golang.org/x/oauth2/google"
	"gopkg.in/Iwark/spreadsheet.v2"
)

func getSheet(credentialsFile string, spreadsheetID string, spreadsheetTitle string) *spreadsheet.Sheet {
	data, err := ioutil.ReadFile(credentialsFile)
	checkError(err)
	conf, err := google.JWTConfigFromJSON(data, spreadsheet.Scope)
	checkError(err)
	client := conf.Client(context.TODO())

	service := spreadsheet.NewServiceWithClient(client)
	spreadsheet, err := service.FetchSpreadsheet(spreadsheetID)
	checkError(err)
	sheet, err := spreadsheet.SheetByTitle(spreadsheetTitle)
	checkError(err)

	return sheet
}
