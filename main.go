package main

import (
	"fmt"
	"time"

	"github.com/BurntSushi/toml"
)

func main() {

	// Define configuration
	type tomlConfig struct {
		CredentialsFile  string
		SpreadsheetID    string
		SpreadsheetTitle string
	}

	// Retrieve onfiguration
	var config tomlConfig
	if _, err := toml.DecodeFile("config.toml", &config); err != nil {
		fmt.Println(err)
		return
	}

	// Get Google sheet object
	sheet := getSheet(
		config.CredentialsFile,
		config.SpreadsheetID,
		config.SpreadsheetTitle,
	)

	// Create list of questions
	questions := [6]string{
		"What is the current date?",
		"What amount did you spend?",
		"Which card did you use?",
		"Who is the vendor?",
		"What category is this purchase?",
		"Notes:",
	}

	// Create list of answers
	answers := make([]string, 6)

	// Collect answers
	for index, value := range questions {

		// Fill in current date for first question
		if index == 0 {
			answers[index] = time.Now().Format("01/02/2006")
			continue
		}

		// For the rest, get input
		fmt.Println(value)
                fmt.Scan(&answers[index])

	}

	// Get current number of rows in sheet
	rows := 0
	for range sheet.Rows {
		rows++
	}

	// Append the answers at the end of the sheet
	rowToAppend := rows
	for index, element := range answers {
		sheet.Update(rowToAppend, index, element)
	}

	// Make sure call Synchronize to reflect the changes
	err := sheet.Synchronize()
	checkError(err)
}

func checkError(err error) {
	if err != nil {
		panic(err)
	}
}
